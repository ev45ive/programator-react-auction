
git clone https://bitbucket.org/ev45ive/programator-react-auction/

# Create

create-react-app nazwa_aplikacji --template=typescript

npm install --save theme-ui json-server react-router-dom express  mongoose react-router-dom

npm install --save-dev @types/express @types/theme-ui @types/react-router-dom @types/react @types/react-dom

https://github.com/system-ui/theme-ui/blob/master/examples/create-react-app/src/App.js


# Stan aplikacji

1. State - setState()
2. Parent state - props.someCallback => setState() w rodzicu
3. ContextAPI
4. localStorage / sessions storeage
5. Server REST API



# Json Server Node Express Typescript
https://github.com/typicode/json-server#custom-routes-example
server/index.ts

# Typescritp COmpiler
tsc server/index.ts
node server/index.js

# TS-Node - Node with auto-compiler
npm i ts-node --save 
ts-node server/index.ts

# TS-Node-Dev - Node with auto-compiler + Auto Restart
npm i ts-node-dev --save 
ts-node-dev --respawn server/index.ts

# File upload
https://stackoverflow.com/questions/58256238/upload-files-with-express-js-and-express-fileupload

