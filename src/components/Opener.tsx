import React, { useEffect } from 'react';
import { useState, FC } from 'react';

export interface Props {
  title: string;
  opened?: boolean
}

const Opener: FC<Props> = (props) => {

  const [opened, setOpened] = useState(true);

  // useEffect(() => {
  //   debugger
  //   setOpened(props.opened)
  // }, [props.opened])

  const toggle = () => setOpened(!opened);

  return (<div className="card my-3">
    <div className="card-header" onClick={toggle}>
      {props.title}
    </div>

    {opened && <div className="card-body">
      {props.children}
    </div>}

  </div>);
};

export default Opener;
