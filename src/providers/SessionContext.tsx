import { useState } from "react"
import React from "react"

export const sessionContext = React.createContext({
  sessionId: '',
  setSesionId: (id: string) => { },
  user: null,
  setUser(user: any) { }
})

export const SessionProvider = (props) => {
  const [sessionId, setSesionId] = useState('')
  const [user, setUser] = useState('')

  return <sessionContext.Provider value={{ sessionId, setSesionId, user, setUser }}>
    {props.children}
  </sessionContext.Provider>
}