import React, { FC, useState, useEffect, useContext } from "react";
import { Auction } from '../models/Auction'
import { sessionContext } from "./SessionContext";

export const CartContext = React.createContext({
  items: [] as CartItem[],
  addToCart: (product: Auction) => { },
  removeItem: (id: number) => { }
})

interface CartItem {
  id: number;
  userId: number;
  productId: number;
  readonly product: Auction;
}

interface CartState {
  items: CartItem[];
  total: number
}

const initialCart: CartState = {
  items: [], total: 0
}


const CartProvider = (props) => {

  const [cartState, setCartState] = useState(initialCart)

  const { sessionId, user } = useContext(sessionContext)

  useEffect(() => {
    fetchCartItems(user.id, sessionId)
      .then(cart_items => setCartState({
        ...cartState,
        items: cart_items
      }));
  }, [user.id])


  const addToCart = (product: Auction) => {
    if (cartState.items.find(p => p.id == product.id)) return;
    createCartItem(product, sessionId).then(refreshItemsList)
  }

  const removeItem = (id: CartItem['id']) => {
    removeCartItem(id, sessionId).then(refreshItemsList)
  }

  return (
    <CartContext.Provider value={{
      items: cartState.items,
      addToCart,
      removeItem
    }
    }>
      {props.children}
    </CartContext.Provider>
  )


  function refreshItemsList() {
    fetchCartItems(user.id, sessionId)
      .then(cart_items => setCartState({
        ...cartState,
        items: cart_items
      }))
  }
}

export default CartProvider


function createCartItem(product: Auction, sessionId) {
  return fetch('http://localhost:9000/cart_items', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Authorization: sessionId
    },
    body: JSON.stringify({
      productId: product.id
    })
  })
    .then(r => r.json())
}

function removeCartItem(itemId: CartItem['id'], sessionId) {
  return fetch('http://localhost:9000/cart_items/' + itemId, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Authorization: sessionId
    }
  })
    .then(r => r.json())
}

function fetchCartItems(userId, sessionId) {
  return fetch('http://localhost:9000/cart_items?_expand=product&userId=' + userId, {
    headers: {
      Authorization: sessionId
    }
  }).then(r => r.json())
}

