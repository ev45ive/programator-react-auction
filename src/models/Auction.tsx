export interface Auction {
  id: number;
  title: string;
  descr: string;
  image: string;
  price: number;
}
