

import React from 'react'
import { RouteComponentProps, useParams } from 'react-router-dom'

interface Props extends RouteComponentProps<{ id: string }> { }

const AuctionDetailView = (props: Props) => {

  const onAddToCart = (id: number) => { }

  // const id = props.match.params.id; // Only when inRoute
  const { id } = useParams()

  const auction = {
    id: 123, title: 'Test 123', descr: 'Opis', price: 120,
    image: 'https://i.picsum.photos/id/36/200/200.jpg'
  };

  return (
    <div>
      <div className="row">
        <div className="col">
          <img src={auction.image} className="w-25" />

        </div>
        <div className="col">
          <h5 className="card-title">
            {auction.title} {id}
          </h5>

          {auction.descr}
          {auction.price}

          <button onClick={e => onAddToCart(auction.id)}
            className="float-right btn btn-success">
            + Add
          </button>
        </div>
      </div>
    </div>
  )
}

export default AuctionDetailView
