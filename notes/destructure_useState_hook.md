```js
cart = {
    status:'new',
    items:[
        {id:123, name:'test123'},
        {id:123, name:'test123'},
    ]
}
// {status: "new", items: Array(2)}

// ==
var status = cart.status
var items = cart.items


var {status, items} =  cart


// ==

var {status, items: cartItems } =  cart

cartItems
// (2) [{…}, {…}]0: {id: 123, name: "test123"}1: {id: 123, name: "test123"} 



function mojState(){ return ['placki', function(){} ] }


var [state, setState ] = mojState()

```
